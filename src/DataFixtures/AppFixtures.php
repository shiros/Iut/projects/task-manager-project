<?php

    namespace App\DataFixtures;


    use App\Entity\Feature\Feature;
    use App\Entity\Project\Project;
    use App\Entity\Project\State;
    use App\Entity\Task\Tag;
    use App\Entity\Task\Task;
    use App\Entity\User\Role;
    use App\Entity\User\User;
    use App\Service\Exception\ExceptionHandlerService;
    use Doctrine\Bundle\FixturesBundle\Fixture;
    use Doctrine\Common\Persistence\ObjectManager;
    use Doctrine\ORM\EntityManagerInterface;

    class AppFixtures extends Fixture
    {
	    /** @var EntityManagerInterface */
	    protected $em;
	    
    	/** @var ExceptionHandlerService */
		protected $exceptionHandlerService;
    	
	    /**
	     * AppFixtures constructor.
	     */
	    public function __construct(EntityManagerInterface $em, ExceptionHandlerService $exceptionHandlerService)
	    {
	    	$this->em = $em;
	    	$this->exceptionHandlerService = $exceptionHandlerService;
	    }
    	
        public function load(ObjectManager $manager)
        {
	        // ----------------
	        // User
	        
	        $user = new User();
	        $user
	            ->setUsername('admin')
		        ->setPlainPassword('admin')
		        ->setRole(Role::ADMIN)
	        ;
	
	
	        // ----------------
	        // Project
	
	        $project = new Project();
	        $project
		        ->setCode('Luna')
		        ->setState(State::OPEN)
		        ->addUser($user)
	        ;
	
	
	        // ----------------
	        // Tag
	
	        $tagAnomalie = new Tag();
	        $tagAnomalie
		        ->setType('Anomalie')
		        ->setColor('#ff0000')
	        ;
	        
	        $tagDeveloppement = new Tag();
	        $tagDeveloppement
	            ->setType('Développement')
		        ->setColor('#80ffff')
	        ;
	        
	        $tagCommentaire = new Tag();
	        $tagCommentaire
		        ->setType('Commentaire')
		        ->setColor('#c0c0c0')
	        ;
	
	        // ----------------
			// Features
	        
	        $featureVarName = 'feature';
	        
	        for ($i = 0; $i < 10; $i++) {
	        	$varNameFeature = $featureVarName . $i;
	        	
	        	$feature = new Feature();
		        $feature
		            ->setTitle("Title Feature n°{$i}")
			        ->setPrice(mt_rand(1, 999) / mt_rand(1, 5))
		        ;
		
		        // ----------------
		        // Tasks
		
		        $taskVarName = 'task';
		
		        for ($j = 0; $j < 10; $j++) {
			        $varNameTask = $taskVarName . $j;
			
			        $task = new Task();
			        $task
				        ->setTitle("Title Task n°{$j} For Feature {$feature->getTitle()}")
				        ->setStartDate(mt_rand(time(), time() + 2592000))
				        ->setEndDate(mt_rand(time() + 86400, time() + 2592000))
				        ->setCode("Code Task n°{$j}")
			        ;
			
			        $rand = mt_rand(1,3);
			
			        switch ($rand) {
				        case 1 :
					        $task->setTag($tagDeveloppement);
					        break;
				
				        case 2 :
					        $task->setTag($tagAnomalie);
					        break;
				
				        case 3 :
					        $task->setTag($tagCommentaire);
					        break;
				
				        default :
					        $task->setTag($tagDeveloppement);
					        break;
			        }
			
			        if ($j > 0) {
				        $previousTaskVarName = $taskVarName . ($j -1);
				        $task->setPreviousTask($$previousTaskVarName);
			        }
			
			        $$varNameTask = $task;
			        $feature->addTask($$varNameTask);
		        }
		
		        $$varNameFeature = $feature;
		        $project->addFeature($$varNameFeature);
	        }
	        
	        try {
		        $manager->persist($tagAnomalie);
		        $manager->persist($tagCommentaire);
		        $manager->persist($tagDeveloppement);
		
		        $manager->persist($project);
		
		        $manager->flush();
	        } catch (\Throwable $throwable) {
	        	$this->exceptionHandlerService->treatment($throwable);
	        }
        }
    }
?>