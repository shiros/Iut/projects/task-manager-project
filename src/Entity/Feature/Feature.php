<?php
    
    namespace App\Entity\Feature;
    
    use App\Entity\Project\Project;
    use App\Entity\Task\Task;
    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\Common\Collections\Collection;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\Feature\FeatureRepository")
     */
    class Feature
    {
        /**
         * @ORM\Id()
         * @ORM\GeneratedValue()
         * @ORM\Column(type="integer")
         */
        protected $id;
    
        /**
         * @ORM\Column(type="string", length=255)
         */
        protected $title;
    
        /**
         * @ORM\Column(type="float")
         */
        protected $price;
	
	    /** @var int */
	    protected $duration;


        # -------------------------------------------------------------
        #   Association
        # -------------------------------------------------------------

        /**
         * @var Project
         * @ORM\ManyToOne(targetEntity="App\Entity\Project\Project", inversedBy="features")
         */
        protected $project;

        /**
         * @var Collection
         * @ORM\OneToMany(targetEntity="App\Entity\Task\Task", mappedBy="feature", cascade={"persist", "remove"})
         */
        protected $tasks;
	
	    /**
	     * Feature constructor.
	     */
	    public function __construct()
	    {
	    	$this->tasks = new ArrayCollection();
	    	
		    $this->duration = 0;
	    }
	
	
	    # -------------------------------------------------------------
        #   Id
        # -------------------------------------------------------------

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }


        # -------------------------------------------------------------
        #   Title
        # -------------------------------------------------------------

        /**
         * @return null|string
         */
        public function getTitle(): ?string
        {
            return $this->title;
        }

        /**
         * @param string $title
         * @return Feature
         */
        public function setTitle(string $title): self
        {
            $this->title = $title;
    
            return $this;
        }


        # -------------------------------------------------------------
        #   Price
        # -------------------------------------------------------------

        /**
         * @return float|null
         */
        public function getPrice(): ?float
        {
            return $this->price;
        }

        /**
         * @param float $price
         * @return Feature
         */
        public function setPrice(float $price): self
        {
            $this->price = $price;
    
            return $this;
        }
	
	
	    # -------------------------------------------------------------
	    #   Duration
	    # -------------------------------------------------------------
	
	    /**
	     * @return int
	     */
	    public function getDuration(): int
	    {
	    	return is_null($this->duration) ? 0 : $this->duration;
	    }
	
	    /**
	     * @param int $duration
	     *
	     * @return self
	     */
	    public function setDuration(int $duration): self
	    {
		    $this->duration = $duration;
		    return $this;
	    }
        
        


        # -------------------------------------------------------------
        #   Project
        # -------------------------------------------------------------

        /**
         * @return Project
         */
        public function getProject()
        {
            return $this->project;
        }

        /**
         * @param Project $project
         * @return self
         */
        public function setProject(Project $project): self
        {
            $this->project = $project;

            return $this;
        }


        # -------------------------------------------------------------
        #   Tasks
        # -------------------------------------------------------------

        /**
         * @return Collection
         */
        public function getTasks()
        {
            return $this->tasks;
        }

        /**
         * @param Task $task
         * @return self
         */
        public function addTask(Task $task): self
        {
        	$task->setFeature($this);
            $this->tasks->add($task);

            return $this;
        }

        /**
         * @param Collection $tasks
         */
        public function setTasks(Collection $tasks)
        {
            $this->tasks = $tasks;
        }
    }
?>