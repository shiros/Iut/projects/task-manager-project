<?php

    namespace App\Entity\Project;

    class State
    {
	    public const STATES = [
		    self::OPEN => 'Ouvert',
		    self::ABORT => 'Fermé',
		    self::CLOSE => 'Clos'
	    ];
	
	    public const OPEN = 'STATE_OPEN';
	    public const ABORT = 'STATE_ABORT';
	    public const CLOSE = 'STATE_CLOSE';
    }
?>