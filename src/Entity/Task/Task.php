<?php

    namespace App\Entity\Task;

    use App\Entity\Feature\Feature;
	use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\Task\TaskRepository")
     */
    class Task
    {
        /**
         * @ORM\Id()
         * @ORM\GeneratedValue()
         * @ORM\Column(type="integer")
         */
        protected $id;

        /**
         * @ORM\Column(type="string", length=255)
         */
        protected $title;
	
	    /**
	     * @ORM\Column(type="integer")
	     */
	    protected $startDate;
	
	    /**
	     * @ORM\Column(type="integer")
	     */
	    protected $endDate;

        /**
         * @ORM\Column(type="string", length=255)
         */
        protected $code;


        # -------------------------------------------------------------
        #   Association
        # -------------------------------------------------------------

        /**
         * @var Feature
         * @ORM\ManyToOne(targetEntity="App\Entity\Feature\Feature", inversedBy="tasks")
         */
        protected $feature;

        /**
         * @var Tag
         * @ORM\ManyToOne(targetEntity="App\Entity\Task\Tag", inversedBy="tasks")
         */
        protected $tag;
	
	    /**
	     * @var Task
	     * @ORM\OneToOne(targetEntity="App\Entity\Task\Task")
	     * @ORM\JoinColumn(name="previous_task", referencedColumnName="id")
	     */
	    protected $previousTask;


        # -------------------------------------------------------------
        #   Id
        # -------------------------------------------------------------

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }
	
	
	    # -------------------------------------------------------------
	    #   Start Date
	    # -------------------------------------------------------------
	
	    /**
	     * @return mixed
	     */
	    public function getStartDate()
	    {
		    return $this->startDate;
	    }
	
	    /**
	     * @param mixed $startDate
	     *
	     * @return self
	     */
	    public function setStartDate($startDate): self
	    {
		    $this->startDate = $startDate;
		    return $this;
	    }
	
	
	    # -------------------------------------------------------------
	    #   End Date
	    # -------------------------------------------------------------
	
	    /**
	     * @return mixed
	     */
	    public function getEndDate()
	    {
		    return $this->endDate;
	    }
	
	    /**
	     * @param mixed $endDate
	     *
	     * @return self
	     */
	    public function setEndDate($endDate): self
	    {
		    $this->endDate = $endDate;
		    return $this;
	    }

        


        # -------------------------------------------------------------
        #   Title
        # -------------------------------------------------------------

        /**
         * @return null|string
         */
        public function getTitle(): ?string
        {
            return $this->title;
        }

        /**
         * @param string $title
         * @return Task
         */
        public function setTitle(string $title): self
        {
            $this->title = $title;

            return $this;
        }


        # -------------------------------------------------------------
        #   Code
        # -------------------------------------------------------------

        /**
         * @return null|string
         */
        public function getCode(): ?string
        {
            return $this->code;
        }

        /**
         * @param string $code
         * @return Task
         */
        public function setCode(string $code): self
        {
            $this->code = $code;

            return $this;
        }


        # -------------------------------------------------------------
        #   Feature
        # -------------------------------------------------------------

        /**
         * @return Feature
         */
        public function getFeature()
        {
            return $this->feature;
        }

        /**
         * @param Feature $feature
         * @return self
         */
        public function setFeature($feature): self
        {
            $this->feature = $feature;

            return $this;
        }
	
	
	    # -------------------------------------------------------------
	    #   Tag
	    # -------------------------------------------------------------
	
	    /**
	     * @return Tag
	     */
	    public function getTag(): ?Tag
	    {
		    return $this->tag;
	    }
	
	    /**
	     * @param Tag $tag
	     *
	     * @return self
	     */
	    public function setTag($tag): self
	    {
		    $this->tag = $tag;
		    return $this;
	    }
	
	
	    # -------------------------------------------------------------
	    #   Task
	    # -------------------------------------------------------------
		
	    /**
	     * @return Task
	     */
	    public function getPreviousTask(): ?Task
	    {
		    return $this->previousTask;
	    }
	
	    /**
	     * @param Task $previousTask
	     *
	     * @return self
	     */
	    public function setPreviousTask(Task $previousTask): self
	    {
		    $this->previousTask = $previousTask;
		    return $this;
	    }
    }
?>
