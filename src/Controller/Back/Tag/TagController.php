<?php

    namespace App\Controller\Back\Tag;

    use App\Controller\AbstractController;
    use App\Entity\Task\Tag;
    use App\Form\Check\Tag\TagCheck;
    use App\Form\Type\Tag\TagType;
    use App\Service\Tag\TagService;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

    /**
     * Class TaskController
     * @package App\Controller\Back\Task
     *
     * @Route("/backoffice/tags")
     */
    class TagController extends AbstractController
    {
	    /**
	     * @var TagService
	     */
	    protected $tagService;
	
	    /**
	     * HomeController constructor.
	     *
	     * @param TagService $tagService
	     */
	    public function __construct(TagService $tagService)
	    {
		    $this->tagService = $tagService;
	    }
	
	    /**
	     * @Route("/", name="bo_tag_index")
	     *
	     * @param Request $request
	     *
	     * @return Response
	     */
	    public function index(Request $request): Response
	    {
		    $tags = $this->tagService->findAll();
		
		    $vars = compact('tags');
		    return $this->render('Back/Tag/index.html.twig', $vars);
	    }
	
	    /**
	     * @Route("add", name="bo_tag_add")
	     *
	     * @param Request $request
	     *
	     * @return Response
	     */
	    public function add(Request $request): Response
	    {
		    $form = $this->createForm(TagType::class);
		    $form->handleRequest($request);
		    $formView = $form->createView();
		
		    $errors = $this->getFormErrors($form);
		
		    if ($this->isPostRequest($request, $form)) {
			    try {
				    $datas = $form->getData();
				    $this->tagService->create($datas);
				
				    return $this->redirectToRoute('bo_tag_index');
			    } catch (\Throwable $throwable) {
				    $errors = $this->tagService->getFormErrors();
				    if (!is_a($throwable, BadRequestHttpException::class)) {
					    array_push($errors, $throwable->getMessage());
				    }
			    }
		    }
		
		    $var = compact('formView', 'errors');
		    return $this->render('Back/Tag/edit.html.twig', $var);
	    }
	
	    /**
	     * @Route("/edit/{id}", name="bo_tag_edit")
	     *
	     * @param Request $request
	     * @param Tag $tag
	     *
	     * @return Response
	     */
	    public function edit(Request $request, Tag $tag): Response
	    {
		    $edit = true;
		    $tagEdit = [];
		
		    $tagEdit[TagCheck::PARAM_TYPE] = $tag->getType();
		    $tagEdit[TagCheck::PARAM_COLOR] = $tag->getColor();
		
		    $form = $this->createForm(TagType::class, $tagEdit, ['edit' => $edit]);
		    $form->handleRequest($request);
		    $formView = $form->createView();
		
		    $errors = $this->getFormErrors($form);
		
		    if ($this->isPostRequest($request, $form)) {
			    try {
				    $datas = $form->getData();
				    $this->tagService->edit($tag, $datas);
				
				    return $this->redirectToRoute('bo_tag_index');
			    } catch (\Throwable $throwable) {
				    $errors = $this->tagService->getFormErrors();
				    if (!is_a($throwable, BadRequestHttpException::class)) {
					    array_push($errors, $throwable->getMessage());
				    }
			    }
		    }
		
		    $var = compact('tag', 'edit', 'formView', 'errors');
		    return $this->render('Back/Tag/edit.html.twig', $var);
	    }
	
	    /**
	     * @Route("/delete/{id}", name="bo_tag_delete")
	     *
	     * @param Request $request
	     * @param Tag $tag
	     *
	     * @return Response
	     */
	    public function delete(Request $request, Tag $tag): Response
	    {
		    $this->tagService->delete($tag);
		    return $this->redirectToRoute('bo_tag_index');
	    }
    }
?>