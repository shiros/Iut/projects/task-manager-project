<?php

    namespace App\Controller\Back\Project;

    use App\Controller\AbstractController;
    use App\Entity\Project\Project;
    use App\Form\Check\Project\ProjectCheck;
    use App\Form\Check\Project\ProjectManageUserCheck;
    use App\Form\Type\Project\ProjectManageUserType;
    use App\Form\Type\Project\ProjectType;
    use App\Service\Project\ProjectService;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

    /**
     * Class ProjectController
     * @package App\Controller\Back\Project
     *
     * @Route("/backoffice/projects")
     */
    class ProjectController extends AbstractController
    {
	    /**
	     * @var ProjectService
	     */
    	protected $projectService;
    	
	    /**
	     * HomeController constructor.
	     */
	    public function __construct(ProjectService $projectService)
	    {
	    	$this->projectService = $projectService;
	    }
	
	    /**
	     * @Route("/", name="bo_project_index")
	     *
	     * @param Request $request
	     *
	     * @return Response
	     */
	    public function index(Request $request): Response
	    {
		    $projects = $this->projectService->findAll();
		
		    $vars = compact('projects');
		    return $this->render('Back/Project/index.html.twig', $vars);
	    }
	
	    /**
         * @Route("/add", name="bo_project_add")
         *
         * @param Request $request
         *
         * @return Response
         */
        public function add(Request $request): Response
        {
	        $form = $this->createForm(ProjectType::class);
	        $form->handleRequest($request);
	        $formView = $form->createView();
	
	        $errors = $this->getFormErrors($form);
	
	        if ($this->isPostRequest($request, $form)) {
		        try {
			        $datas = $form->getData();
			        $this->projectService->create($datas);
			
			        return $this->redirectToRoute('bo_project_index');
		        } catch (\Throwable $throwable) {
			        $errors = $this->projectService->getProjectFormErrors();
			        if (!is_a($throwable, BadRequestHttpException::class)) {
				        array_push($errors, $throwable->getMessage());
			        }
		        }
	        }
	
	        $var = compact('formView', 'errors');
	        return $this->render('Back/Project/edit.html.twig', $var);
        }
	
	    /**
	     * @Route("/edit/{id}", name="bo_project_edit")
	     *
	     * @param Request $request
	     *
	     * @return Response
	     */
	    public function edit(Request $request, Project $project): Response
	    {
		    $edit = true;
		    $projectEdit = [];
		    
		    $projectEdit[ProjectCheck::PARAM_CODE] = $project->getCode();
		    $projectEdit[ProjectCheck::PARAM_STATE] = $project->getState();
	    	
		    $form = $this->createForm(ProjectType::class, $projectEdit, ['edit' => $edit]);
		    $form->handleRequest($request);
		    $formView = $form->createView();
		
		    $errors = $this->getFormErrors($form);
		
		    if ($this->isPostRequest($request, $form)) {
			    try {
				    $datas = $form->getData();
				    $this->projectService->edit($project, $datas);
				
				    return $this->redirectToRoute('bo_project_index');
			    } catch (\Throwable $throwable) {
				    $errors = $this->projectService->getProjectFormErrors();
				    if (!is_a($throwable, BadRequestHttpException::class)) {
					    array_push($errors, $throwable->getMessage());
				    }
			    }
		    }
		
		    $var = compact('project', 'edit', 'formView', 'errors');
		    return $this->render('Back/Project/edit.html.twig', $var);
	    }
	
	    /**
	     * @Route("/{id}/manage", name="bo_project_manage_user")
	     *
	     * @param Request $request
	     *
	     * @return Response
	     */
	    public function manageUser(Request $request, Project $project): Response
	    {
		    $projectManageUser[ProjectManageUserCheck::PARAM_USERS] = $project->getUsers()->toArray();
		
		    $form = $this->createForm(ProjectManageUserType::class, $projectManageUser, ['project' => $project]);
		    $form->handleRequest($request);
		    $formView = $form->createView();
		
		    $errors = $this->getFormErrors($form);
		
		    if ($this->isPostRequest($request, $form)) {
			    try {
				    $datas = $form->getData();
				    $this->projectService->manageUser($project, $datas);
				
				    return $this->redirectToRoute('bo_project_edit', ['id' => $project->getId()]);
			    } catch (\Throwable $throwable) {
				    $errors = $this->projectService->getManageUserFormErrors();
				    if (!is_a($throwable, BadRequestHttpException::class)) {
					    array_push($errors, $throwable->getMessage());
				    }
			    }
		    }
		
		    $var = compact('project', 'formView', 'errors');
		    return $this->render('Back/Project/manageUser.html.twig', $var);
	    }
	
	    /**
	     * @Route("/delete/{id}", name="bo_project_delete")
	     *
	     * @param Request $request
	     *
	     * @return Response
	     */
	    public function delete(Request $request, Project $project): Response
	    {
	    	$this->projectService->delete($project);
		    return $this->redirectToRoute('bo_project_index');
	    }
    }
?>