<?php

    namespace App\Controller\Back\Task;

    use App\Controller\AbstractController;
    use App\Entity\Feature\Feature;
    use App\Entity\Task\Task;
    use App\Form\Check\Task\TaskCheck;
    use App\Form\Type\Task\TaskType;
    use App\Service\Task\TaskService;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

    /**
     * Class TaskController
     * @package App\Controller\Back\Task
     *
     * @Route("/backoffice")
     */
    class TaskController extends AbstractController
    {
	    /**
	     * @var TaskService
	     */
	    protected $taskService;
	
	    /**
	     * HomeController constructor.
	     *
	     * @param TaskService $taskService
	     */
	    public function __construct(TaskService $taskService)
	    {
		    $this->taskService = $taskService;
	    }
	
	    /**
	     * @Route("/features/{id}/task/add", name="bo_task_add")
	     *
	     * @param Request $request
	     * @param Feature $feature
	     *
	     * @return Response
	     */
	    public function add(Request $request, Feature $feature): Response
	    {
		    $form = $this->createForm(TaskType::class, [], ['feature' => $feature]);
		    $form->handleRequest($request);
		    $formView = $form->createView();
		
		    $errors = $this->getFormErrors($form);
		
		    if ($this->isPostRequest($request, $form)) {
			    try {
				    $datas = $form->getData();
				    $this->taskService->create($feature, $datas);
				
				    return $this->redirectToRoute('bo_feature_edit', ['id' => $feature->getId()]);
			    } catch (\Throwable $throwable) {
				    $errors = $this->taskService->getFormErrors();
				    if (!is_a($throwable, BadRequestHttpException::class)) {
					    array_push($errors, $throwable->getMessage());
				    }
			    }
		    }
		
		    $var = compact('feature', 'formView', 'errors');
		    return $this->render('Back/Task/edit.html.twig', $var);
	    }
	
	    /**
	     * @Route("/tasks/edit/{id}", name="bo_task_edit")
	     *
	     * @param Request $request
	     * @param Task $task
	     *
	     * @return Response
	     */
	    public function edit(Request $request, Task $task): Response
	    {
		    $feature = $task->getFeature();
		
		    $edit = true;
		    $taskEdit = [];
		
		    $taskEdit[TaskCheck::PARAM_TITLE] = $task->getTitle();
		    $taskEdit[TaskCheck::PARAM_START_DATE] = $task->getStartDate();
		    $taskEdit[TaskCheck::PARAM_END_DATE] = $task->getEndDate();
		    $taskEdit[TaskCheck::PARAM_CODE] = $task->getCode();
		    $taskEdit[TaskCheck::PARAM_TAG] = $task->getTag();
		    $taskEdit[TaskCheck::PARAM_PREVIOUS_TASK] = $task->getPreviousTask();
		
		    $form = $this->createForm(TaskType::class, $taskEdit, ['feature' => $feature, 'edit' => $edit]);
		    $form->handleRequest($request);
		    $formView = $form->createView();
		
		    $errors = $this->getFormErrors($form);
		
		    if ($this->isPostRequest($request, $form)) {
			    try {
				    $datas = $form->getData();
				    $this->taskService->edit($task, $datas);
				
				    return $this->redirectToRoute('bo_feature_edit', ['id' => $feature->getId()]);
			    } catch (\Throwable $throwable) {
				    $errors = $this->taskService->getFormErrors();
				    if (!is_a($throwable, BadRequestHttpException::class)) {
					    array_push($errors, $throwable->getMessage());
				    }
			    }
		    }
		
		    $var = compact('feature', 'task', 'edit', 'formView', 'errors');
		    return $this->render('Back/Task/edit.html.twig', $var);
	    }
	
	    /**
	     * @Route("/tasks/delete/{id}", name="bo_task_delete")
	     *
	     * @param Request $request
	     * @param Task $task
	     *
	     * @return Response
	     */
	    public function delete(Request $request, Task $task): Response
	    {
		    $feature = $task->getFeature();
		
		    $this->taskService->delete($task);
		    return $this->redirectToRoute('bo_feature_edit', ['id' => $feature->getId()]);
	    }
    }
?>