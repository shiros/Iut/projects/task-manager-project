<?php

    namespace App\Controller\Back;

    use App\Controller\AbstractController;
    use App\Service\Project\ProjectService;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;

    /**
     * Class HomeController
     * @package App\Controller\Back
     *
     * @Route("/backoffice")
     */
    class HomeController extends AbstractController
    {
	    /**
	     * @var ProjectService
	     */
    	protected $projectService;
    	
	    /**
	     * HomeController constructor.
	     */
	    public function __construct(ProjectService $projectService)
	    {
	    	$this->projectService = $projectService;
	    }
	
	    /**
         * @Route("/", name="bo_index")
         *
         * @param Request $request
         *
         * @return Response
         */
        public function index(Request $request): Response
        {
        	$menus = [
        		'Gestion des Utilisateurs' => [
        		    'icon' => 'group',
                    'url' => 'bo_user_index'
                ],
		        'Gestion des Projets' => [
                    'icon' => 'work_outline',
		            'url' => 'bo_project_index'
                ],
		        'Gestion des Tags' => [
			        'icon' => 'loyalty',
			        'url' => 'bo_tag_index'
		        ]
	        ];
        	
        	$vars = compact('menus');
        	return $this->render('Back/index.html.twig', $vars);
        }
    }
?>