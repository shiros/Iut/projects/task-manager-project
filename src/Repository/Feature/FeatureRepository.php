<?php

namespace App\Repository\Feature;

use App\Entity\Feature\Feature;
use App\Entity\Task\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class FeatureRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Feature::class);
    }
	
	/**
	 * @param mixed $id
	 * @param null $lockMode
	 * @param null $lockVersion
	 *
	 * @return Feature|null|object
	 */
	public function find($id, $lockMode = null, $lockVersion = null)
	{
		/** @var Feature $feature */
		$feature = parent::find($id, $lockMode, $lockVersion);
		
		if (!is_null($feature)) {
			$this->prepareFeature($feature);
		}
		
		return $feature;
	}
	
	/**
	 * @param array $criteria
	 * @param array|NULL $orderBy
	 *
	 * @return Feature|null|object
	 */
	public function findOneBy(array $criteria, array $orderBy = null)
	{
		/** @var Feature $feature */
		$feature = parent::findOneBy($criteria, $orderBy);
		
		if (!is_null($feature)) {
			$this->prepareFeature($feature);
		}
		
		return $feature;
	}
	
	/**
	 * @return array
	 */
	public function findAll()
    {
    	$features = parent::findAll();
	
	    /** @var Feature $feature */
    	foreach ($features as $feature) {
    		$this->prepareFeature($feature);
	    }
	    
	    return $features;
    }
	
	/**
	 * @param array $criteria
	 * @param array|NULL $orderBy
	 * @param null $limit
	 * @param null $offset
	 *
	 * @return array
	 */
	public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
	{
		$features = parent::findBy($criteria, $orderBy, $limit, $offset);
		
		/** @var Feature $feature */
		foreach ($features as $feature) {
			$this->prepareFeature($feature);
		}
		
		return $features;
	}
    
	/**
	 * Prepare some calculated vars for feature (Example : Duration)
	 *
	 * @param Feature $feature
	 */
	public function prepareFeature(Feature $feature)
	{
		$tasks = $feature->getTasks();
		
		/** @var Task $task */
		foreach ($tasks as $task) {
			$start = $task->getStartDate();
			$end = $task->getEndDate();
			
			$featureDuration = $feature->getDuration();
			$timeSpent = $end - $start;
			
			$feature->setDuration($featureDuration + $timeSpent);
		}
	}
}
