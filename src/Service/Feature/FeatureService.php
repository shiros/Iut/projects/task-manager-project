<?php

    namespace App\Service\Feature;

    use App\Entity\Feature\Feature;
    use App\Entity\Project\Project;
    use App\Form\Check\Feature\FeatureCheck;
    use App\Repository\Feature\FeatureRepository;
    use App\Service\Exception\ExceptionHandlerService;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

    /**
     * Class FeatureService
     * @package App\Service\Feature
     */
    class FeatureService
    {
        /** @var EntityManagerInterface */
        protected $em;

        /** @var FeatureRepository */
        protected $repository;
	
	    /** @var FeatureCheck */
	    protected $check;

        /** @var ExceptionHandlerService */
        protected $exceptionHandlerService;
	
	    /**
	     * UserService constructor.
	     *
	     * @param EntityManagerInterface $em
	     * @param FeatureCheck $check
	     * @param ExceptionHandlerService $exceptionHandlerService
	     */
        public function __construct(EntityManagerInterface $em, FeatureCheck $check, ExceptionHandlerService $exceptionHandlerService)
        {
            $this->em = $em;
            $this->repository = $em->getRepository(Feature::class);
	
	        $this->check = $check;
            $this->exceptionHandlerService = $exceptionHandlerService;
        }

        /**
         * @param Feature $feature
         * @return Feature
         */
        protected function persist(Feature $feature): Feature
        {
            try {
                $this->em->persist($feature);
                $this->em->flush();
            } catch (\Throwable $throwable) {
                $this->exceptionHandlerService->treatment($throwable);
            }

            return $feature;
        }

        /**
         * @return array
         */
        public function getFormErrors(): array
        {
            return $this->check->getErrors();
        }
	
	
	    # -------------------------------------------------------------
	    #   Operation CRUD
	    # -------------------------------------------------------------
		
	    /**
	     * @return array
	     */
	    public function findAll(): array
	    {
	    	return $this->repository->findAll();
	    }
	    

        /**
         * @param Project $project
         * @param array $datas
         *
         * @return Feature
         */
        public function create(Project $project, array $datas): Feature
        {
            $this->check->create($datas);

            if (!$this->check->isValid()) {
                throw new BadRequestHttpException('Some parameters are missing');
            }

            $feature = new Feature();
	        $feature
                ->setTitle($datas[FeatureCheck::PARAM_TITLE])
		        ->setPrice($datas[FeatureCheck::PARAM_PRICE])
            ;

	        $project->addFeature($feature);
	        
            return $this->persist($feature);
        }
	
	    /**
	     * @param Feature $feature
	     * @param array $datas
	     *
	     * @return Feature
	     */
	    public function edit(Feature $feature, array $datas): Feature
	    {
		    if (array_key_exists(FeatureCheck::PARAM_TITLE, $datas)) {
		    	$title = $datas[FeatureCheck::PARAM_TITLE];
		    	
			    if (!is_null($title)) {
				    $feature->setTitle($title);
			    }
		    }
		
		    if (array_key_exists(FeatureCheck::PARAM_PRICE, $datas)) {
			    $price = $datas[FeatureCheck::PARAM_PRICE];
			
			    if (!is_null($title) and is_float($price)) {
				    $feature->setPrice($price);
			    }
		    }
		
		    try {
			    $this->em->flush();
		    } catch (\Throwable $throwable) {
			    $this->exceptionHandlerService->treatment($throwable);
		    }
		
		    return $feature;
	    }

        /**
         * @param Feature $feature
         */
        public function delete(Feature $feature)
        {
        	try {
                $this->em->remove($feature);
                $this->em->flush();
            } catch (\Throwable $throwable) {
                $this->exceptionHandlerService->treatment($throwable);
            }
        }
    }

?>