<?php

    namespace App\Service\Tag;
    
    use App\Entity\Task\Tag;
    use App\Form\Check\Tag\TagCheck;
    use App\Repository\Task\TagRepository;
    use App\Service\Exception\ExceptionHandlerService;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


    /**
     * Class TaskService
     * @package App\Service\Task
     */
    class TagService
    {
        /** @var EntityManagerInterface */
        protected $em;

        /** @var TagRepository */
        protected $repository;
	
	    /** @var TagCheck */
	    protected $check;

        /** @var ExceptionHandlerService */
        protected $exceptionHandlerService;
	
	    /**
	     * UserService constructor.
	     *
	     * @param EntityManagerInterface $em
	     * @param TagCheck $check
	     * @param ExceptionHandlerService $exceptionHandlerService
	     */
        public function __construct(EntityManagerInterface $em, TagCheck $check, ExceptionHandlerService $exceptionHandlerService)
        {
            $this->em = $em;
            $this->repository = $em->getRepository(Tag::class);
	
	        $this->check = $check;
            $this->exceptionHandlerService = $exceptionHandlerService;
        }

        /**
         * @param Tag $tag
         * @return Tag
         */
        protected function persist(Tag $tag): Tag
        {
            try {
                $this->em->persist($tag);
                $this->em->flush();
            } catch (\Throwable $throwable) {
                $this->exceptionHandlerService->treatment($throwable);
            }

            return $tag;
        }

        /**
         * @return array
         */
        public function getFormErrors(): array
        {
            return $this->check->getErrors();
        }
	
	
	    # -------------------------------------------------------------
	    #   Operation CRUD
	    # -------------------------------------------------------------
		
	    /**
	     * @return array
	     */
	    public function findAll(): array
	    {
	    	return $this->repository->findAll();
	    }
	    

        /**
         * @param array $datas
         *
         * @return Tag
         */
        public function create(array $datas): Tag
        {
            $this->check->create($datas);

            if (!$this->check->isValid()) {
                throw new BadRequestHttpException('Some parameters are missing');
            }

            $tag = new Tag();
	        $tag
                ->setType($datas[TagCheck::PARAM_TYPE])
		        ->setColor($datas[TagCheck::PARAM_COLOR])
            ;
	        
            return $this->persist($tag);
        }
	
	    /**
	     * @param Tag $tag
	     * @param array $datas
	     *
	     * @return Tag
	     */
	    public function edit(Tag $tag, array $datas): Tag
	    {
		    if (array_key_exists(TagCheck::PARAM_TYPE, $datas)) {
		    	$type = $datas[TagCheck::PARAM_TYPE];
		    	
			    if (!is_null($type)) {
				    $tag->setType($type);
			    }
		    }
		
		    if (array_key_exists(TagCheck::PARAM_COLOR, $datas)) {
			    $color = $datas[TagCheck::PARAM_COLOR];
			
			    if (!is_null($color)) {
				    $tag->setColor($color);
			    }
		    }
		
		    try {
			    $this->em->flush();
		    } catch (\Throwable $throwable) {
			    $this->exceptionHandlerService->treatment($throwable);
		    }
		
		    return $tag;
	    }

        /**
         * @param Tag $tag
         */
        public function delete(Tag $tag)
        {
        	try {
                $this->em->remove($tag);
                $this->em->flush();
            } catch (\Throwable $throwable) {
                $this->exceptionHandlerService->treatment($throwable);
            }
        }
    }

?>