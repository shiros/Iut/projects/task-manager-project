<?php

    namespace App\Service\Task;
    use App\Entity\Feature\Feature;
    use App\Entity\Task\Task;
    use App\Form\Check\Task\TaskCheck;
    use App\Repository\Task\TaskRepository;
    use App\Service\Exception\ExceptionHandlerService;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


    /**
     * Class TaskService
     * @package App\Service\Task
     */
    class TaskService
    {
        /** @var EntityManagerInterface */
        protected $em;

        /** @var TaskRepository */
        protected $repository;
	
	    /** @var TaskCheck */
	    protected $check;

        /** @var ExceptionHandlerService */
        protected $exceptionHandlerService;
	
	    /**
	     * UserService constructor.
	     *
	     * @param EntityManagerInterface $em
	     * @param TaskCheck $check
	     * @param ExceptionHandlerService $exceptionHandlerService
	     */
        public function __construct(EntityManagerInterface $em, TaskCheck $check, ExceptionHandlerService $exceptionHandlerService)
        {
            $this->em = $em;
            $this->repository = $em->getRepository(Task::class);
	
	        $this->check = $check;
            $this->exceptionHandlerService = $exceptionHandlerService;
        }

        /**
         * @param Task $task
         * @return Task
         */
        protected function persist(Task $task): Task
        {
            try {
                $this->em->persist($task);
                $this->em->flush();
            } catch (\Throwable $throwable) {
                $this->exceptionHandlerService->treatment($throwable);
            }

            return $task;
        }

        /**
         * @return array
         */
        public function getFormErrors(): array
        {
            return $this->check->getErrors();
        }
	
	
	    # -------------------------------------------------------------
	    #   Operation CRUD
	    # -------------------------------------------------------------
		
	    /**
	     * @return array
	     */
	    public function findAll(): array
	    {
	    	return $this->repository->findAll();
	    }
	    

        /**
         * @param Feature $feature
         * @param array $datas
         *
         * @return Task
         */
        public function create(Feature $feature, array $datas): Task
        {
            $this->check->create($datas);

            if (!$this->check->isValid()) {
                throw new BadRequestHttpException('Some parameters are missing');
            }

            $task = new Task();
	        $task
                ->setTitle($datas[TaskCheck::PARAM_TITLE])
		        ->setStartDate($datas[TaskCheck::PARAM_START_DATE])
		        ->setEndDate($datas[TaskCheck::PARAM_END_DATE])
		        ->setCode($datas[TaskCheck::PARAM_CODE])
		        ->setTag($datas[TaskCheck::PARAM_TAG])
            ;
	        
	        if (array_key_exists(TaskCheck::PARAM_PREVIOUS_TASK, $datas)) {
	        	$previousTask = $datas[TaskCheck::PARAM_PREVIOUS_TASK];
	        	if (!is_null($previousTask)) {
	        		$task->setPreviousTask($previousTask);
		        }
	        }

	        $feature->addTask($task);
	        
            return $this->persist($task);
        }
	
	    /**
	     * @param Task $task
	     * @param array $datas
	     *
	     * @return Task
	     */
	    public function edit(Task $task, array $datas): Task
	    {
		    if (array_key_exists(TaskCheck::PARAM_TITLE, $datas)) {
		    	$title = $datas[TaskCheck::PARAM_TITLE];
		    	
			    if (!is_null($title)) {
				    $task->setTitle($title);
			    }
		    }
		
		    if (array_key_exists(TaskCheck::PARAM_START_DATE, $datas)) {
			    $startDate = $datas[TaskCheck::PARAM_START_DATE];
			
			    if (!is_null($startDate)) {
				    $task->setStartDate($startDate);
			    }
		    }
		
		    if (array_key_exists(TaskCheck::PARAM_END_DATE, $datas)) {
			    $endDate = $datas[TaskCheck::PARAM_END_DATE];
			
			    if (!is_null($endDate)) {
				    $task->setEndDate($endDate);
			    }
		    }
		
		    if (array_key_exists(TaskCheck::PARAM_CODE, $datas)) {
			    $code = $datas[TaskCheck::PARAM_CODE];
			
			    if (!is_null($code)) {
				    $task->setCode($code);
			    }
		    }
		
		    if (array_key_exists(TaskCheck::PARAM_TAG, $datas)) {
			    $tag = $datas[TaskCheck::PARAM_TAG];
			
			    if (!is_null($tag)) {
				    $task->setTag($tag);
			    }
		    }
		
		    if (array_key_exists(TaskCheck::PARAM_PREVIOUS_TASK, $datas)) {
			    $previousTask = $datas[TaskCheck::PARAM_PREVIOUS_TASK];
			
			    if (!is_null($previousTask)) {
				    $task->setPreviousTask($previousTask);
			    }
		    }
		
		    try {
			    $this->em->flush();
		    } catch (\Throwable $throwable) {
			    $this->exceptionHandlerService->treatment($throwable);
		    }
		
		    return $task;
	    }

        /**
         * @param Task $task
         */
        public function delete(Task $task)
        {
        	try {
                $this->em->remove($task);
                $this->em->flush();
            } catch (\Throwable $throwable) {
                $this->exceptionHandlerService->treatment($throwable);
            }
        }
    }

?>