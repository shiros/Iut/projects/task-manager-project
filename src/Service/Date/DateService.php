<?php

    namespace App\Service\Date;


    /**
     * Class DateService
     * @package App\Service\Date
     */
    class DateService
    {
		public function display(int $time)
		{
			$duration = '';
			
			$days = floor($time / 86400);
			$time -= $days * 86400;
			
			$hours = floor($time / 3600);
			$time -= $hours * 3600;
			
			$minutes = floor($time / 60);
			$seconds = $time - $minutes * 60;
			
			if($days > 0) {
				$duration .= "{$days} Jours,";
			}
			
			if($hours > 0) {
				$duration .= " {$hours} Heures,";
			}
			
			if($minutes > 0) {
				$duration .= " {$minutes} Minutes";
			}
			
			/*
			if($seconds > 0) {
				$duration .= " {$seconds} Secondes";
			}
			*/
			
			return $duration;
		}
    }
?>