<?php

    namespace App\Form\Type\Project;

    use App\Entity\Project\Project;
    use App\Entity\User\User;
    use App\Form\Check\Project\ProjectManageUserCheck;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Bridge\Doctrine\Form\Type\EntityType;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\Exception\HttpException;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class ProjectManageUserType extends AbstractType
    {
	    public static $idProject;
	
	    public function buildForm(FormBuilderInterface $builder, array $options)
        {
        	/** @var Project $project */
	        $project = $options['project'];
	
	        if (is_null($project) or !is_a($project, Project::class)) {
		        throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, "Project in ProjectManageUserType is missing.");
	        }
	        
	        self::$idProject = $project->getId();
        	
            $builder
	            ->add(ProjectManageUserCheck::PARAM_USERS,
		            EntityType::class,
		            [
			            'class'        => User::class,
			            'query_builder' => function (EntityRepository $er) {
				            $params = [
					            'project_id' => self::$idProject,
				            ];
				
				            $rawQuery = $er
					            ->createQueryBuilder('U')
					            ->where("U.project IS NULL OR U.project = :project_id")
				            ;
				
				            $prepareQuery = $rawQuery->setParameters($params);
				            return $prepareQuery;
			            },
			            'label'        => 'Utilisateurs :',
                        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
			            'choice_label' => 'username',
			            'multiple'     => true,
			            'expanded'     => true,
			            'required'     => false,
		            ])
            ;

            $builder
	            ->add('validate',
	                SubmitType::class,
	                [
	                    'label'  => 'Valider',
                        'attr'   => ['class' => 'btn btn-primary d-block d-center w-auto mtb-10']
	                ])
            ;
        }

        public function configureOptions(OptionsResolver $resolver)
        {
            $resolver->setDefaults([
                'data_class' => NULL,
                'project'    => NULL
            ]);
        }
    }
?>