<?php

    namespace App\Form\Type\Project;

    use App\Entity\Project\State;
    use App\Form\Check\Project\ProjectCheck;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\TimeType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class ProjectType extends AbstractType
    {
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $edit = $options['edit'];
            $buttonId = 'edit';
            $buttonName = 'Valider';

            if (!$edit) {
                $buttonId = 'create';
                $buttonName = 'Créer';

                $builder
                    ->add(ProjectCheck::PARAM_CODE,
                        TextType::class,
                        [
                            'label'      => 'Code du Project :',
                            'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
                            'attr'       => ['class' => 'form-control'],
                            'required'   => true
                        ])
                ;
            }

            $builder
	            ->add(ProjectCheck::PARAM_STATE,
		            ChoiceType::class,
		            [
			            'choices'    => array_flip(State::STATES),
			            'label'      => 'Etat :',
                        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
                        'attr'       => ['class' => 'custom-select custom-select-lg mb-3'],
			            'multiple'   => false,
			            'expanded'   => false,
			            'required'   => false,
		            ])
            ;

            $builder->add($buttonId,
                SubmitType::class,
                [
                    'label'      => $buttonName,
                    'attr'       => ['class' => 'btn btn-primary d-block d-center w-auto mtb-10']
                ]);
        }

        public function configureOptions(OptionsResolver $resolver)
        {
            $resolver->setDefaults([
                'data_class' => NULL,
                'edit'       => false
            ]);
        }
    }
?>