<?php

    namespace App\Form\Type\Task;

    use App\Entity\Feature\Feature;
    use App\Entity\Task\Tag;
    use App\Entity\Task\Task;
    use App\Form\Check\Task\TaskCheck;
    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\Query\Expr\Join;
    use Symfony\Bridge\Doctrine\Form\Type\EntityType;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\Exception\HttpException;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class TaskType extends AbstractType
    {
    	public static $idFeature;
    	
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $edit = $options['edit'];
            
            /** @var Feature $feature */
	        $feature = $options['feature'];
	        
            $buttonId = 'edit';
            $buttonName = 'Valider';

            if (is_null($feature) or !is_a($feature, Feature::class)) {
	            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, "Feature in TaskType is missing.");
            }
            
            self::$idFeature = $feature->getId();
            
            if (!$edit) {
                $buttonId = 'create';
                $buttonName = 'Créer';
            }
	
	        $builder
		        ->add(TaskCheck::PARAM_TITLE,
			        TextType::class,
			        [
				        'label'      => 'Titre :',
				        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
				        'attr'       => ['class' => 'form-control'],
				        'required'   => true
			        ])
		        ->add(TaskCheck::PARAM_START_DATE,
			        DateTimeType::class,
			        [
				        'label'       => 'Date de début :',
				        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
				        'attr'       => ['class' => 'form-control'],
				        'date_widget' => "single_text",
				        'input'       => 'timestamp',
				        'required'    => true,
			        ])
		        ->add(TaskCheck::PARAM_END_DATE,
			        DateTimeType::class,
			        [
				        'label'       => 'Date de fin :',
				        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
				        'attr'       => ['class' => 'form-control'],
				        'date_widget' => "single_text",
				        'input'       => 'timestamp',
				        'required'    => true,
			        ])
		        ->add(TaskCheck::PARAM_CODE,
			        TextType::class,
			        [
				        'label'      => 'Code :',
				        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
				        'attr'       => ['class' => 'form-control'],
				        'required'   => true
			        ])
		        ->add(TaskCheck::PARAM_TAG,
			        EntityType::class,
			        [
				        'class'        => Tag::class,
				        'choice_label' => 'type',
				        'label'        => 'Tag :',
				        'label_attr'   => ['class' => 'mtb-5 text-blue text-italic text-bold'],
				        'attr'         => ['class' => 'custom-select custom-select-lg mb-3'],
				        'multiple'     => false,
				        'expanded'     => false,
				        'required'     => false,
			        ])
		        ->add(TaskCheck::PARAM_PREVIOUS_TASK,
			        EntityType::class,
			        [
				        'class'        => Task::class,
				        'query_builder' => function (EntityRepository $er) {
					        $params = [
						        'id' => self::$idFeature,
					        ];
					        
		        	        $rawQuery = $er
				                ->createQueryBuilder('T')
				                ->leftJoin(Feature::class, 'F', Join::WITH, 'F.id = T.feature')
				                ->where("F.id = :id")
			                ;
					
					        $prepareQuery = $rawQuery->setParameters($params);
					        return $prepareQuery;
				        },
				        'choice_label' => 'title',
				        'label'        => 'Tâche Parente :',
				        'label_attr'   => ['class' => 'mtb-5 text-blue text-italic text-bold'],
				        'attr'         => ['class' => 'custom-select custom-select-lg mb-3'],
				        'multiple'     => false,
				        'expanded'     => false,
				        'required'     => false,
			        ])
	        ;

            $builder->add($buttonId,
                SubmitType::class,
                [
                    'label'      => $buttonName,
                    'attr'       => ['class' => 'btn btn-primary d-block d-center w-auto mtb-10']
                ]);
        }

        public function configureOptions(OptionsResolver $resolver)
        {
            $resolver->setDefaults([
                'data_class' => NULL,
                'feature'    => NULL,
                'edit'       => false
            ]);
        }
    }
?>