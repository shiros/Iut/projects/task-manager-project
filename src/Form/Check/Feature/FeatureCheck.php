<?php

    namespace App\Form\Check\Feature;

    use App\Form\Check\AbstractCheck;

    class FeatureCheck extends AbstractCheck
    {
        public const PARAM_TITLE = 'title';
        public const PARAM_PRICE = 'price';

        /**
         * @param array $datas
         */
        public function create(array $datas)
        {
            $this->errors = [];

            if (empty($datas)) {
                array_push($this->errors, "Des champs sont manquants");
            }

            // TITLE
            if (!array_key_exists(self::PARAM_TITLE, $datas)) {
                array_push($this->errors, "Le champ 'Titre' ne peut pas être vide");
            } else {
                if (is_null($datas[self::PARAM_TITLE])) {
                    array_push($this->errors, "Le champ 'Titre' ne peut pas être vide");
                }
            }

            // PRICE
            if (!array_key_exists(self::PARAM_PRICE, $datas)) {
                array_push($this->errors, "Le champ 'Prix' ne peut pas être vide");
            } else {
                if (is_null($datas[self::PARAM_PRICE])) {
                    array_push($this->errors, "Le champ 'Prix de réalisation' ne peut pas être vide");
                }
            }
        }
    }
?>