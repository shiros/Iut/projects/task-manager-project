<?php

    namespace App\Form\Check\Task;

    use App\Form\Check\AbstractCheck;

    class TaskCheck extends AbstractCheck
    {
        public const PARAM_TITLE = 'title';
	    public const PARAM_START_DATE = 'start_date';
	    public const PARAM_END_DATE = 'end_date';
        public const PARAM_CODE = 'code';
	    public const PARAM_PREVIOUS_TASK = 'previous_task';
	    public const PARAM_TAG = 'tag';

        /**
         * @param array $datas
         */
        public function create(array $datas)
        {
            $this->errors = [];

            if (empty($datas)) {
                array_push($this->errors, "Des champs sont manquants");
            }

            // TITLE
            if (!array_key_exists(self::PARAM_TITLE, $datas)) {
                array_push($this->errors, "Le champ 'Titre' ne peut pas être vide");
            } else {
                if (is_null($datas[self::PARAM_TITLE])) {
                    array_push($this->errors, "Le champ 'Titre' ne peut pas être vide");
                }
            }

            // START DATE
            if (!array_key_exists(self::PARAM_START_DATE, $datas)) {
                array_push($this->errors, "Le champ 'Date de début' ne peut pas être vide");
            } else {
                if (is_null($datas[self::PARAM_START_DATE])) {
                    array_push($this->errors, "Le champ 'Date de début' ne peut pas être vide");
                }
            }
	
	        // END DATE
	        if (!array_key_exists(self::PARAM_END_DATE, $datas)) {
		        array_push($this->errors, "Le champ 'Date de fin' ne peut pas être vide");
	        } else {
		        if (is_null($datas[self::PARAM_END_DATE])) {
			        array_push($this->errors, "Le champ 'Date de fin' ne peut pas être vide");
		        }
	        }
	
	        // CODE
	        if (!array_key_exists(self::PARAM_CODE, $datas)) {
		        array_push($this->errors, "Le champ 'Code' ne peut pas être vide");
	        } else {
		        if (is_null($datas[self::PARAM_CODE])) {
			        array_push($this->errors, "Le champ 'Code' ne peut pas être vide");
		        }
	        }
	
	        // TAG
	        if (!array_key_exists(self::PARAM_TAG, $datas)) {
		        array_push($this->errors, "Le champ 'Tag' ne peut pas être vide");
	        } else {
		        if (is_null($datas[self::PARAM_TAG])) {
			        array_push($this->errors, "Le champ 'Tag' ne peut pas être vide");
		        }
	        }
        }
    }
?>