<?php

    namespace App\Form\Check\Project;

    use App\Form\Check\AbstractCheck;

    class ProjectCheck extends AbstractCheck
    {
        public const PARAM_CODE = 'code';
        public const PARAM_STATE = 'state';

        /**
         * @param array $datas
         */
        public function create(array $datas)
        {
            $this->errors = [];

            if (empty($datas)) {
                array_push($this->errors, "Des champs sont manquants");
            }

            // CODE
            if (!array_key_exists(self::PARAM_CODE, $datas)) {
                array_push($this->errors, "Le champ 'Code' ne peut pas être vide");
            } else {
                if (is_null($datas[self::PARAM_CODE])) {
                    array_push($this->errors, "Le champ 'Code' ne peut pas être vide");
                }
            }

            // STATE
            if (!array_key_exists(self::PARAM_STATE, $datas)) {
                array_push($this->errors, "Le champ 'Etat' doit être renseigné");
            } else {
                if (empty($datas[self::PARAM_STATE])) {
                    array_push($this->errors, "Le champ 'Etat' ne peut pas être vide");
                }
            }
        }
    }
?>