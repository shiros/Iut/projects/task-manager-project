<?php

    namespace App\Form\Check\Tag;

    use App\Form\Check\AbstractCheck;

    class TagCheck extends AbstractCheck
    {
        public const PARAM_TYPE = 'type';
	    public const PARAM_COLOR = 'color';
	    
        /**
         * @param array $datas
         */
        public function create(array $datas)
        {
            $this->errors = [];

            if (empty($datas)) {
                array_push($this->errors, "Des champs sont manquants");
            }

            // TYPE
            if (!array_key_exists(self::PARAM_TYPE, $datas)) {
                array_push($this->errors, "Le champ 'Type' ne peut pas être vide");
            } else {
                if (is_null($datas[self::PARAM_TYPE])) {
                    array_push($this->errors, "Le champ 'Type' ne peut pas être vide");
                }
            }

            // COLOR
            if (!array_key_exists(self::PARAM_COLOR, $datas)) {
                array_push($this->errors, "Le champ 'Couleur' ne peut pas être vide");
            } else {
                if (is_null($datas[self::PARAM_COLOR])) {
                    array_push($this->errors, "Le champ 'Couleur' ne peut pas être vide");
                }
            }
        }
    }
?>